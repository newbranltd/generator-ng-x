'use strict';
const Generator = require('yeoman-generator');
const chalk = require('chalk');
const yosay = require('yosay');

/**

    1. usual funny greating - check stuff behind the scene
    2. ask them what third party CSS library they want
       a. TBC
    3. create skeleton for the projects
       generators/app/templates/root is all the root level files
       package.json
       shitty.ts files
       rollup.config.js
       gulpfile.js
       etc
       etc
    4. check if they have yarn installed or then run the install for them
    5. run
    6. bye

* */

module.exports = class extends Generator {
  prompting() {
    // Have Yeoman greet the user.
    this.log(yosay(
      'Welcome to the astounding ' + chalk.red('generator-ng-x') + ' generator!'
    ));

    const prompts = [{
      type: 'confirm',
      name: 'someAnswer',
      message: 'Would you like to enable this option?',
      default: true
    }];

    return this.prompt(prompts).then(props => {
      // To access props later use this.props.someAnswer;
      this.props = props;
    });
  }

  writing() {
    this.fs.copy(
      this.templatePath('dummyfile.txt'),
      this.destinationPath('dummyfile.txt')
    );
  }

  install() {
    this.installDependencies();
  }
};
