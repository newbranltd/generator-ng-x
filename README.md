# generator-ng-x [![NPM version][npm-image]][npm-url] [![Build Status][travis-image]][travis-url] [![Dependency Status][daviddm-image]][daviddm-url]
> Angular V.x (4 at the mo) generator with gulp, rollup and stuff

## CURRENTLY UNDERCONSTRUCTION 

## Installation

First, install [Yeoman](http://yeoman.io) and generator-ng-x using [npm](https://www.npmjs.com/) (we assume you have pre-installed [node.js](https://nodejs.org/)).

```bash
npm install -g yo
npm install -g generator-ng-x
```

Then generate your new project:

```bash
yo ng-x
```

## @TODO more sub-generator command and what they do etc





## Getting To Know Yeoman

 * Yeoman has a heart of gold.
 * Yeoman is a person with feelings and opinions, but is very easy to work with.
 * Yeoman can be too opinionated at times but is easily convinced not to be.
 * Feel free to [learn more about Yeoman](http://yeoman.io/).

## License

MIT © [joelchu](https://to1source.com)


[npm-image]: https://badge.fury.io/js/generator-ng-x.svg
[npm-url]: https://npmjs.org/package/generator-ng-x
[travis-image]: https://travis-ci.org/joelchu/generator-ng-x.svg?branch=master
[travis-url]: https://travis-ci.org/joelchu/generator-ng-x
[daviddm-image]: https://david-dm.org/joelchu/generator-ng-x.svg?theme=shields.io
[daviddm-url]: https://david-dm.org/joelchu/generator-ng-x
